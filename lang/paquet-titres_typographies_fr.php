<?php
/** Fichier de langue de SPIP **/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'titres_typographies_slogan'=>'Typographiez vos titres !',
	'titres_typographies_description'=>'Permet d\'utiliser les raccourcis typographiques de SPIP dans les champs TITRE et apparentés.'
	
);

?>