# Changelog

## 1.2.8 - 2024-06-18


### Changed

- Nécessite SPIP 4.2 minimum

### Fixed

- Recalculer les squelettes à l'activation du plugin
- #3 : Supprimer les liens des champs typographiés

## 1.2.7 - 2024-06-17

### Fixed

- #2 : Prendre en compte plus de champs dans les traitements

## 1.2.6 - 2024-05-07

### Fixed

- Compatibilité SPIP 4.y.z

### Removed

- Compatibilité SPIP 3.x

## 1.2.5 - 2023-02-27

### Fixed

- Compatibilité SPIP 4.2
