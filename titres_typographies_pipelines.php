<?php
// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

function titres_typographies_declarer_tables_interfaces($interfaces) {
	if (isset($interfaces['table_des_traitements']['TITRE']['forums'])) {
		$interfaces['table_des_traitements']['TITRE']['forums'] = 'titres_typographies_supprimer_liens(trim(PtoBR(safehtml(' . str_replace('%s', 'interdit_html(%s)', _TRAITEMENT_RACCOURCIS) . '))))';
	}
	$interfaces['table_des_traitements']['NOM_SITE_SPIP'][0] = _TRAITEMENT_TYPO_SANS_NUMERO;
	$interfaces['table_des_traitements']['SLOGAN_SITE_SPIP'][0] = _TRAITEMENT_TYPO_SANS_NUMERO;
	$interfaces['table_des_traitements']['SOUSTITRE'][0] = _TRAITEMENT_TYPO_SANS_NUMERO;
	$interfaces['table_des_traitements']['SURTITRE'][0] = _TRAITEMENT_TYPO_SANS_NUMERO;
	$interfaces['table_des_traitements']['NOM_SITE'][0] = _TRAITEMENT_TYPO_SANS_NUMERO;
	$interfaces['table_des_traitements']['NOM_SITE']['auteurs'] = _TRAITEMENT_TYPO_SANS_NUMERO;
	return $interfaces;
}

/**
 * Prend un texte avec lien et le retourne sans
 * @param string $texte
 * @return string
**/
function titres_typographies_supprimer_liens(string $texte): string {
	if (str_contains($texte, '<a')) {
		$texte = preg_replace('#<a .*?>(.*?)</a>#is', '\1', $texte);
	}

	return $texte;
}
