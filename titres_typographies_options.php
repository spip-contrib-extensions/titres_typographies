<?php
// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

// Pour que le squelette soit recalculé et que l'effet du plugin soit donc visible dans l'admin dès l'installation
$GLOBALS['marqueur_skel'] = ($GLOBALS['marqueur_skel'] ?? '') . ':titres_typographies';

if (!defined('_TRAITEMENT_TYPO_SANS_NUMERO')) {
	define('_TRAITEMENT_TYPO_SANS_NUMERO', 'titres_typographies_supprimer_liens(trim(PtoBR(propre(supprimer_numero(%s), $connect, $Pile[0]))))');
}


